package main

import (
	"fmt"
	"net/http"
	"os"

	"webprogrammieren.de/Thorsten/webprogrammieren.de/classe/seiten"
)

var (
	router = http.NewServeMux()
	server = &http.Server{
		Addr:    Port(),
		Handler: router,
	}
	serverhttps = &http.Server{
		Addr:    ":9000",
		Handler: router,
	}
)

func Port() string {
	if len(os.Args) > 1 {
		fmt.Println("Server läuft auf Port " + os.Args[1])
		return ":" + os.Args[1]
	} else {
		fmt.Println("Server läuft auf Port 8080")
		return ":9000"
	}
}

func main() {
	starten := seiten.Seiten{}
	router.HandleFunc("/", starten.Lesen)
	router.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	if Port() == ":9000" {
		serverhttps.ListenAndServeTLS("/etc/letsencrypt/live/webprogrammieren.de/cert.pem", "/etc/letsencrypt/live/webprogrammieren.de/privkey.pem")
	} else {
		server.ListenAndServe()
	}
}
