package seiten

import (
	"io/ioutil"
	"net/http"
	"text/template"

	"github.com/russross/blackfriday"
	"gopkg.in/yaml.v2"
)

var (
	vorlagen, _ = template.ParseGlob("vorlagen/*")
)

type Seitenyaml struct {
	Titel string `yaml:"Titel"`
}

type Seiten struct {
	SeitenName string
	Titel      string
	Nav        string
	Inhalt     string
}

func (Info *Seiten) Lesen(w http.ResponseWriter, r *http.Request) {

	YamlDateiLesen := Seitenyaml{}
	Seitenausgabe := Seiten{}

	auslesen, _ := ioutil.ReadFile("./Seiten/conf.yml") //Auslesen
	yaml.Unmarshal([]byte(auslesen), &YamlDateiLesen)
	Seitenausgabe.SeitenName = YamlDateiLesen.Titel
	Seitenausgabe.Titel = r.URL.Path[len("/"):]
	if Seitenausgabe.Titel == "" {
		Seitenausgabe.Titel = "index"
	}
	dateiausgabe, _ := ioutil.ReadFile("./Seiten/" + Seitenausgabe.Titel + ".md")
	output := blackfriday.MarkdownCommon(dateiausgabe)
	Seitenausgabe.Inhalt = string(output)
	dateiausgabe_nav, _ := ioutil.ReadFile("./Seiten/" + "nav" + ".md")
	outputnav := blackfriday.MarkdownCommon(dateiausgabe_nav)
	Seitenausgabe.Nav = string(outputnav)
	vorlagen.ExecuteTemplate(w, "index.htm", Seitenausgabe)
}
