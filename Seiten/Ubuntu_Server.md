## webprogrammieren.de
### certbot Installieren
*  [Cerbort](https://certbot.eff.org/)
### MYSQL 8
``` bash
   wget -c dev.mysql.com/get/mysql-apt-config_0.8.12-1_all.deb
   apt-get install  gnupg 
   sudo dpkg -i mysql-apt-config*
   sudo apt update
   sudo apt-get install mysql-server
   sudo systemctl status mysql
   sudo systemctl status mysql
   sudo systemctl enable mysql
   sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
   -------------------Datei mysqld.cnf --------------
   [mysqld]
   bind-address = 0.0.0.0
   -------------------Datei mysqd ende----------------
   mysql -u root -p
   use mysql;
      update user set host='%' where user='eigene Benutzername';
      update db set host='%' where user='euer_benutzer';
      CREATE DATABASE git;
      CREATE DATABASE webprogrammieren.de;
      exit;
      sudo service mysql restart
```
### Apache 2

```
sudo apt-get update
sudo apt-get install apache2  php php-mysql libapache2-mod-php php-xml php-mbstring
sudo apt-get install php-intl imagemagick inkscape php-gd php-cli curl unzip zip git
sudo chmod 777 -R /var/www
sudo rm -r /var/www/html

``` 
### Git
```   
sudo apt-get install git-core
```
### Gitea
``` 

wget -O gitea  https://dl.gitea.io/gitea/1.8/gitea-1.8-linux-amd64
chmod +x gitea
```
### Grav

* [Grav](https://learn.getgrav.org)

```
nano /etc/apache2/sites-available/grav.conf
---Datei--

       <VirtualHost *:8000>
        ServerName www.webprogrammieren.de   
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/grav/
        <Directory /var/www/grav/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>
        ErrorLog /var/log/apache2/error.log
        LogLevel warn
        CustomLog /var/log/apache2/access.log combined
        ServerSignature On
        </VirtualHost>


--Datei Ende--
sudo a2enmod rewrite 
sudo a2ensite grav.conf
/etc/init.d/apache2 restart
```